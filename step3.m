function [p,phi_e,e_a,e_ext,GCI] = step3(h,phi,init_value,e,max_iter)

% comment added on 2015-04-22:
% Describe the input arguments.
% h - the averaged element length, row vector, the first element has the
% smallest length
% phi - the concerned physical value, row vector
% init_value - the initial guess of p
% e - the maximum residual of p
% max_iter - the maximum number of allowed iteration when compute p
%
% NOTE: The user should check if this function show a warning.

% modified on 2015-04-23
% Give the user a warning if the relative values of esp21 or esp32 
% is to close to zero.

% modified on 2015-04-23
% Try Git.

% mofified on 2015-04-27
% The program will not warn the user if the values of relativeEps21 and
% relattiveEps32 are too close.
%

CLOSE_TO_ZERO_LIMIT = 0.05;


r21 = 1.0*h(2)/h(1);
r32 = 1.0*h(3)/h(2);

eps21 = phi(2) - phi(1);
eps32 = phi(3) - phi(2);

minPhi = min(phi);

relativeEps21 = abs(eps21 / minPhi);
relativeEps32 = abs(eps32 / minPhi);

% check if either relativeEps21 or relativeEps32 is close to zero
% if (relativeEps21 <= CLOSE_TO_ZERO_LIMIT ||...
%     relativeEps32 <= CLOSE_TO_ZERO_LIMIT)
if (0)
    fprintf('=============== WARNING ===========\n');
    fprintf('The values in phi are too close to each other!\n');
    fprintf('The limit is set to %f. The relative ratio of eps21 and eps32 are %f and %f, respectively.\n',...
        CLOSE_TO_ZERO_LIMIT, relativeEps21, relativeEps32);
    fprintf('============ END OF WARNING ========\n\n');
else
    fprintf('relativeEps21 = %f\nrelatvieEps32 = %f\n\n', ...
        relativeEps21, relativeEps32);
end

s = sign(1.0*eps32/eps21);

current_e = 1;
current_step = 1;

p = init_value;

temp_2 = log(abs(1.0*eps32/eps21));

while (abs(current_e) >= e && current_step <= max_iter)
    [q,qp] = getQ(r21,r32,s,p);
    
    temp = temp_2 + q;
    f  = 1.0/log(r21) * abs(temp) - p;
    
    if (temp >= 0)
        fp = 1.0/log(r21) * qp - 1;
    else
        fp = 1.0/log(r21) * (-1) * qp - 1;
    end
    
    current_e = f/fp;
    
    p = p - current_e;
    
    disp(['Current step = ',num2str(current_step),' current error = ',num2str(current_e)]);
    current_step = current_step + 1;
end

phi_e = [0,0];
e_a   = phi_e;
e_ext = phi_e;
GCI   = phi_e;

phi_e(1) = (r21^p*phi(1) - phi(2)) / (r21^p - 1);
phi_e(2) = (r32^p*phi(2) - phi(3)) / (r32^p - 1);

e_a(1) = abs((phi(1)-phi(2))/phi(1));
e_a(2) = abs((phi(2)-phi(3))/phi(2));

e_ext(1) = abs((phi_e(1)-phi(1))/phi_e(1));
e_ext(2) = abs((phi_e(2)-phi(2))/phi_e(2));

GCI(1) = 1.25*e_a(1) / (r21^p-1);
GCI(2) = 1.25*e_a(2) / (r32^p-1);

end