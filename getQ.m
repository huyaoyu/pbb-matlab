function [q,qp] = getQ(r21,r32,s,p)

t32 = (r32^p-s);
t21 = (r21^p-s);

q  = log(t21/t32);

qp = t32/t21 * (r21^p*log(r21)*t32 - t21*r32^p*log(r32)) / t32^2;
end