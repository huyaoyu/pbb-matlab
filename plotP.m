function plotP(h,phi,p_span,num_points)

r21 = 1.0*h(2)/h(1);
r32 = 1.0*h(3)/h(2);

eps21 = phi(2) - phi(1);
eps32 = phi(3) - phi(2);

s = sign(1.0*eps32/eps21);

p_step = (p_span(2) - p_span(1)) / (num_points-1);

f = [];

for p = p_span(1):p_step:p_span(2)
    [q,qp] = getQ(r21,r32,s,p);
    temp = log(abs(eps32/eps21))+q;
    f  = [f,1.0/log(r21) * abs(temp) - p];
end

plot(p_span(1):p_step:p_span(2),f);